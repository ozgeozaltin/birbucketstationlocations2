define('bitbucket/internal/page/dashboard/dashboard', ['exports', 'react', 'react-dom', 'react-redux', 'bitbucket/internal/feature/dashboard/dashboard', 'bitbucket/internal/feature/dashboard/reducers/paging', 'bitbucket/internal/feature/dashboard/reducers/repositories', 'bitbucket/internal/feature/dashboard/reducers/ui', 'bitbucket/internal/util/redux'], function (exports, _react, _reactDom, _reactRedux, _dashboard, _paging, _repositories, _ui, _redux) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.onReady = onReady;

    var _react2 = babelHelpers.interopRequireDefault(_react);

    var _reactDom2 = babelHelpers.interopRequireDefault(_reactDom);

    var _dashboard2 = babelHelpers.interopRequireDefault(_dashboard);

    var _paging2 = babelHelpers.interopRequireDefault(_paging);

    var _repositories2 = babelHelpers.interopRequireDefault(_repositories);

    var _ui2 = babelHelpers.interopRequireDefault(_ui);

    function onReady(el, user) {

        var store = (0, _redux.customCreateStore)({
            paging: _paging2.default,
            repositories: _repositories2.default,
            ui: _ui2.default
        });

        _reactDom2.default.render(_react2.default.createElement(
            _reactRedux.Provider,
            { store: store },
            _react2.default.createElement(_dashboard2.default, { user: user })
        ), el);
    }
});