define('bitbucket/internal/feature/dashboard/components/repository-search', ['exports', 'react', 'react-redux', 'bitbucket/internal/impl/web-fragments', '../actions', '../repository-type'], function (exports, _react, _reactRedux, _webFragments, _actions, _repositoryType) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.RepositorySearch = undefined;

    var _react2 = babelHelpers.interopRequireDefault(_react);

    var _webFragments2 = babelHelpers.interopRequireDefault(_webFragments);

    var SEARCH_LOCATION = 'internal.bitbucket.dashboard.repository.search';

    var RepositorySearch = exports.RepositorySearch = function (_Component) {
        babelHelpers.inherits(RepositorySearch, _Component);

        function RepositorySearch(props) {
            babelHelpers.classCallCheck(this, RepositorySearch);

            var _this = babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(RepositorySearch).call(this, props));

            _this.state = {
                Search: null
            };
            return _this;
        }

        babelHelpers.createClass(RepositorySearch, [{
            key: 'componentWillMount',
            value: function componentWillMount() {
                var panels = _webFragments2.default.getWebPanels(SEARCH_LOCATION);
                if (panels.length > 0) {
                    this.setState({
                        // getWebPanels in the SPI version of webFragments (as opposed to WebFragments global) returns the result
                        // of calling the view function as .html, but really it's just whatever the "view" in the client-web-panel
                        // returns, which in this case is a React component
                        Search: panels[0].html
                    });
                }
            }
        }, {
            key: 'onSearchStart',
            value: function onSearchStart(query) {
                this.props.dispatch({
                    type: _actions.LOAD_REPOSITORIES,
                    meta: {
                        repoType: _repositoryType.SEARCH,
                        query: query
                    }
                });
            }
        }, {
            key: 'onSearchEnd',
            value: function onSearchEnd(query) {
                // TODO ???
            }
        }, {
            key: 'onSearchClear',
            value: function onSearchClear() {
                this.props.dispatch({
                    type: _actions.CLEAR_REPOSITORIES,
                    meta: {
                        repoType: _repositoryType.SEARCH
                    }
                });
            }
        }, {
            key: 'onSearchError',
            value: function onSearchError() {
                this.props.dispatch({
                    type: _actions.LOAD_REPOSITORIES_FAILURE
                });
            }
        }, {
            key: 'onSearchResults',
            value: function onSearchResults(query, page, nextSearch) {
                this.props.dispatch({
                    type: _actions.LOAD_REPOSITORIES_SUCCESS,
                    payload: page,
                    meta: {
                        repoType: _repositoryType.SEARCH,
                        nextSearch: nextSearch,
                        query: query
                    }
                });
            }
        }, {
            key: 'render',
            value: function render() {
                var _this2 = this;

                var Search = this.state.Search;

                return Search ? _react2.default.createElement(Search, {
                    onSearchStart: function onSearchStart(query) {
                        return _this2.onSearchStart(query);
                    },
                    onSearchEnd: function onSearchEnd(query) {
                        return _this2.onSearchEnd(query);
                    },
                    onSearchResults: function onSearchResults(query, page, nextSearch) {
                        return _this2.onSearchResults(query, page, nextSearch);
                    },
                    onSearchError: function onSearchError() {
                        return _this2.onSearchError();
                    },
                    onSearchClear: function onSearchClear() {
                        return _this2.onSearchClear();
                    },
                    pageSize: 25 /* FIXME pass a default page size */
                }) : null;
            }
        }]);
        return RepositorySearch;
    }(_react.Component);

    exports.default = (0, _reactRedux.connect)()(RepositorySearch);
});