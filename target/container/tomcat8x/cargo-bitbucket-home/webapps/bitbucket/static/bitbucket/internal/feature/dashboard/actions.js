define('bitbucket/internal/feature/dashboard/actions', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var prefixed = function prefixed(action) {
    return 'dashboard/' + action;
  };

  var LOAD_REPOSITORIES = exports.LOAD_REPOSITORIES = prefixed('LOAD_REPOSITORIES');
  var LOAD_REPOSITORIES_SUCCESS = exports.LOAD_REPOSITORIES_SUCCESS = prefixed('LOAD_REPOSITORIES_SUCCESS');
  var LOAD_REPOSITORIES_FAILURE = exports.LOAD_REPOSITORIES_FAILURE = prefixed('LOAD_REPOSITORIES_FAILURE');
  var CLEAR_REPOSITORIES = exports.CLEAR_REPOSITORIES = prefixed('CLEAR_REPOSITORIES');
});