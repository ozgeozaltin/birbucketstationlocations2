'use strict';

define('bitbucket/internal/page/pull-request/view/pull-request-view-diff', ['jquery', 'bitbucket/util/state', 'bitbucket/internal/feature/pull-request/pull-request-diff', 'bitbucket/internal/model/page-state'], function ($, state, pullRequestDiffFeature, pageState) {
    //Persist the commit without exposing it in other tabs
    var currentCommit = void 0;
    var currentCommitRange = void 0;

    return {
        load: function load(el) {
            pageState.setCommit(pageState.getCommit() || currentCommit);
            pageState.setCommitRange(pageState.getCommitRange() || currentCommitRange);

            el.innerHTML = bitbucket.internal.feature.pullRequest.diff({
                'commit': state.getCommit(),
                'repository': state.getRepository()
            });
            pullRequestDiffFeature.init({
                commit: state.getCommit(),
                commitRange: pageState.getCommitRange(),
                currentUser: pageState.getCurrentUser(),
                pullRequest: pageState.getPullRequest(),
                maxChanges: pageState.getPullRequestViewInternal().maxChanges,
                relevantContextLines: pageState.getPullRequestViewInternal().relevantContextLines,
                seenCommitReview: pageState.getPullRequestViewInternal().seenCommitReview,
                showUnreviewedChanges: pageState.getPullRequestViewInternal().showUnreviewedChanges
            });
        },
        unload: function unload(el) {
            currentCommit = pageState.getCommit();
            pageState.setCommit(null);
            currentCommitRange = pageState.getCommitRange();
            pageState.setCommitRange(null);
            return pullRequestDiffFeature.reset().done(function () {
                //Don't empty the el until the promise is done to avoid blowing away any data needed for cleanup
                $(el).empty();
            });
        },
        keyboardShortcutContexts: ['diff-tree', 'diff-view']
    };
});