define("bitbucket/internal/feature/dashboard/dashboard", ["module", "exports", "react", "./repositories"], function (module, exports, _react, _repositories) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = babelHelpers.interopRequireDefault(_react);

    var _repositories2 = babelHelpers.interopRequireDefault(_repositories);

    var Dashboard = function (_Component) {
        babelHelpers.inherits(Dashboard, _Component);

        function Dashboard() {
            babelHelpers.classCallCheck(this, Dashboard);
            return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(Dashboard).apply(this, arguments));
        }

        babelHelpers.createClass(Dashboard, [{
            key: "render",
            value: function render() {
                return _react2.default.createElement(_repositories2.default, null);
            }
        }]);
        return Dashboard;
    }(_react.Component);

    exports.default = Dashboard;
    module.exports = exports["default"];
});