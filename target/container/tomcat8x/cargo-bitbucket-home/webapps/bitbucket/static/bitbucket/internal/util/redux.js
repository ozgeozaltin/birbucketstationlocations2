define('bitbucket/internal/util/redux', ['exports', 'redux', 'bitbucket/internal/bbui/utils/promise-middleware', 'bitbucket/internal/bbui/utils/thunk-middleware'], function (exports, _redux, _promiseMiddleware, _thunkMiddleware) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.customCreateStore = customCreateStore;

    var _promiseMiddleware2 = babelHelpers.interopRequireDefault(_promiseMiddleware);

    var _thunkMiddleware2 = babelHelpers.interopRequireDefault(_thunkMiddleware);

    var loggerMiddleware = function loggerMiddleware(store) {
        return function (next) {
            return function (action) {
                console.group(action.type);
                console.log('dispatching', action);
                var result = next(action);
                console.log('next state', JSON.parse(JSON.stringify(store.getState())));
                console.groupEnd(action.type);
                return result;
            };
        };
    };

    var middleware = [_thunkMiddleware2.default, _promiseMiddleware2.default];

    // if url params has dev=true
    if (/[?&]dev=true($|&)/.test(window.location.search)) {
        middleware.push(loggerMiddleware);
    }

    /**
     * Create a Redux Store with middleware applied by default.
     *
     * @param {Object} reducers - an object of reducers to combine and pass along to the store middleware
     * @param {*} defaultState - the default state/data of the store.
     * @returns {Redux.Store}
     */
    function customCreateStore(reducers, defaultState) {
        return (0, _redux.createStore)((0, _redux.combineReducers)(reducers), defaultState, (0, _redux.compose)(_redux.applyMiddleware.apply(undefined, middleware), window.devToolsExtension ? window.devToolsExtension() : function (f) {
            return f;
        }));
    }
});