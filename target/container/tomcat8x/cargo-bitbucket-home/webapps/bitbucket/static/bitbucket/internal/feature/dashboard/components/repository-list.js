define('bitbucket/internal/feature/dashboard/components/repository-list', ['module', 'exports', 'react', 'bitbucket/util/navbuilder', 'bitbucket/internal/bbui/aui-react/avatar', 'bitbucket/internal/model-transformer'], function (module, exports, _react, _navbuilder, _avatar, _modelTransformer) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = babelHelpers.interopRequireDefault(_react);

    var _navbuilder2 = babelHelpers.interopRequireDefault(_navbuilder);

    var _modelTransformer2 = babelHelpers.interopRequireDefault(_modelTransformer);

    var Repository = function Repository(_ref) {
        var repository = _ref.repository;

        return _react2.default.createElement(
            'li',
            null,
            _react2.default.createElement(_avatar.ProjectAvatar, { project: _modelTransformer2.default.project(repository.project), size: 'medium' }),
            _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    'a',
                    { href: _navbuilder2.default.project(repository.project).repo(repository).build(), className: 'repository', title: repository.name },
                    _react2.default.createElement(
                        'strong',
                        null,
                        repository.name
                    )
                ),
                _react2.default.createElement(
                    'a',
                    { href: _navbuilder2.default.project(repository.project).build(), className: 'project', title: repository.project.name },
                    repository.project.name
                )
            )
        );
    };
    Repository.displayName = 'Repository';

    // TODO proper empty state
    var NoRepositories = function NoRepositories() {
        return _react2.default.createElement(
            'li',
            { className: 'dashboard-repo-empty' },
            'No repositories found...'
        );
    };
    NoRepositories.displayName = 'NoRepositories';

    exports.default = function (_ref2) {
        var onMoreClick = _ref2.onMoreClick;
        var repositories = _ref2.repositories;
        var showMore = _ref2.showMore;

        return _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
                'ul',
                { id: 'dashboard-repo-list' },
                repositories && (repositories.length > 0 ? repositories.map(function (repo) {
                    return _react2.default.createElement(Repository, { key: repo.id, repository: repo });
                }) : _react2.default.createElement(NoRepositories, null))
            ),
            showMore ? _react2.default.createElement(
                'button',
                {
                    className: 'aui-button aui-button-link',
                    onClick: function onClick() {
                        return onMoreClick();
                    }
                },
                AJS.I18n.getText('bitbucket.web.dashboard.repositories.loadmore')
            ) : null
        );
    };

    module.exports = exports['default'];
});