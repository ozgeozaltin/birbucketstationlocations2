define('bitbucket/internal/feature/dashboard/action-creators/load-recent-repositories', ['module', 'exports', 'bitbucket/util/navbuilder', 'bitbucket/util/server', '../actions', '../repository-type'], function (module, exports, _navbuilder, _server, _actions, _repositoryType) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    exports.default = function () {
        var start = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];

        var repoType = _repositoryType.RECENT;

        var url = _navbuilder2.default.rest().profile().recent().repos().withParams({
            start: start,
            avatarSize: bitbucket.internal.widget.avatarSizeInPx({ size: 'medium' })
        }).build();

        return function (dispatch) {
            dispatch({
                type: _actions.LOAD_REPOSITORIES,
                meta: { repoType: repoType }
            });

            (0, _server.rest)({
                url: url,
                statusCode: {
                    '*': function _() {
                        return false; // don't show any error messages
                    }
                }
            }).done(function (data) {
                dispatch({
                    type: _actions.LOAD_REPOSITORIES_SUCCESS,
                    payload: data,
                    meta: { repoType: repoType }
                });
            }).fail(function (error) {
                dispatch({
                    type: _actions.LOAD_REPOSITORIES_FAILURE,
                    payload: error,
                    meta: { repoType: repoType }
                });
            });
        };
    };

    var _navbuilder2 = babelHelpers.interopRequireDefault(_navbuilder);

    module.exports = exports['default'];
});