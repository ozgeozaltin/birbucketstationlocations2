define('bitbucket/internal/feature/dashboard/reducers/paging', ['module', 'exports', 'lodash', 'bitbucket/internal/bbui/utils/create-reducer', '../actions'], function (module, exports, _lodash, _createReducer2, _actions) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _createReducer3 = babelHelpers.interopRequireDefault(_createReducer2);

    var _createReducer;

    exports.default = (0, _createReducer3.default)({}, (_createReducer = {}, babelHelpers.defineProperty(_createReducer, _actions.LOAD_REPOSITORIES_SUCCESS, function (state, action) {
        var repoType = action.meta.repoType;
        var repositories = void 0;
        var newRepositories = action.payload.values.map(function (repo) {
            return repo.id;
        });

        // if there's a query, and it's different, then REPLACE the repos, else concat
        if ((0, _lodash.get)(action, 'meta.query') !== (0, _lodash.get)(state, [repoType, 'query'])) {
            repositories = newRepositories;
        } else {
            repositories = (0, _lodash.get)(state, [repoType, 'repositories'], []).concat(newRepositories);
        }

        return babelHelpers.extends({}, state, babelHelpers.defineProperty({}, repoType, {
            query: (0, _lodash.get)(action, 'meta.query'),
            lastPageMeta: (0, _lodash.pick)(action.payload, (0, _lodash.without)(Object.keys(action.payload), 'values')),
            repositories: repositories,
            loadMoreCallback: (0, _lodash.get)(action, 'meta.nextSearch')
        }));
    }), babelHelpers.defineProperty(_createReducer, _actions.CLEAR_REPOSITORIES, function (state, action) {
        return babelHelpers.extends({}, state, babelHelpers.defineProperty({}, action.meta.repoType, undefined));
    }), _createReducer));
    module.exports = exports['default'];
});