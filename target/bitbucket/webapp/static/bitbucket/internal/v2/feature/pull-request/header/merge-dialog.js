define('bitbucket/internal/v2/feature/pull-request/header/merge-dialog', ['module', 'exports', 'aui', 'jquery', 'bitbucket/util/navbuilder', 'bitbucket/internal/model-transformer', 'bitbucket/internal/model/page-state', 'bitbucket/internal/util/ajax', 'bitbucket/internal/util/events', 'bitbucket/internal/widget/submit-spinner', './get-action-url', './set-dialog-buttons-disabled', './update-pull-request'], function (module, exports, _aui, _jquery, _navbuilder, _modelTransformer, _pageState, _ajax, _events, _submitSpinner, _getActionUrl, _setDialogButtonsDisabled, _updatePullRequest) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _aui2 = babelHelpers.interopRequireDefault(_aui);

    var _jquery2 = babelHelpers.interopRequireDefault(_jquery);

    var _navbuilder2 = babelHelpers.interopRequireDefault(_navbuilder);

    var _modelTransformer2 = babelHelpers.interopRequireDefault(_modelTransformer);

    var _pageState2 = babelHelpers.interopRequireDefault(_pageState);

    var _ajax2 = babelHelpers.interopRequireDefault(_ajax);

    var _events2 = babelHelpers.interopRequireDefault(_events);

    var _submitSpinner2 = babelHelpers.interopRequireDefault(_submitSpinner);

    var _getActionUrl2 = babelHelpers.interopRequireDefault(_getActionUrl);

    var _setDialogButtonsDisabled2 = babelHelpers.interopRequireDefault(_setDialogButtonsDisabled);

    var _updatePullRequest2 = babelHelpers.interopRequireDefault(_updatePullRequest);

    var DEFAULT_MERGE_TIMEOUT_SEC = 5 * 60;
    var MERGE_STRATEGY_BUTTON_SELECTOR = '#merge-strategy-button';
    var mergeDialog = void 0;

    function initMergeDialog(props) {
        _ajax2.default.rest({
            url: _navbuilder2.default.rest().currentRepo().pullRequestSettings().build(),
            type: 'GET'
        }).done(function (PullRequestJSON) {
            var pullRequest = _pageState2.default.getPullRequest();
            var filterStrategies = PullRequestJSON.mergeConfig.strategies.filter(function (strategy) {
                // filter out disabled strategies
                return strategy.enabled;
            }).filter(function (strategy) {
                // remove the default strategy
                return strategy.id !== PullRequestJSON.mergeConfig.defaultStrategy.id;
            });

            filterStrategies.unshift(PullRequestJSON.mergeConfig.defaultStrategy); // add default to beginning

            var options = {
                dialog: {
                    person: _pageState2.default.getCurrentUser() && _pageState2.default.getCurrentUser().toJSON(),
                    pullRequest: pullRequest.toJSON(),
                    mergeStrategies: filterStrategies,
                    defaultStrategy: PullRequestJSON.mergeConfig.defaultStrategy
                },
                ajax: {
                    statusCode: {
                        '400': function _(xhr, textStatus, errorThrown, response, dominantError) {
                            var $mergeDialogContent = mergeDialog.$el.find('.aui-dialog2-content');

                            if (response.errors) {
                                $mergeDialogContent.children('.aui-message').remove();
                                $mergeDialogContent.prepend(bitbucket.internal.feature.pullRequest.merge.errors({ 'errors': response.errors }));
                            }

                            return false;
                        },
                        '401': function _(xhr, textStatus, errorThrown, errors, dominantError) {
                            mergeDialog.hide();
                            return babelHelpers.extends({}, dominantError, {
                                title: _aui2.default.I18n.getText('bitbucket.web.pullrequest.merge.error.401.title'),
                                message: _aui2.default.I18n.getText('bitbucket.web.pullrequest.merge.error.401.message'),
                                fallbackUrl: false,
                                shouldReload: true
                            });
                        },
                        '409': function _(xhr, textStatus, errorThrown, response, dominantError) {
                            var firstError = response.errors && response.errors.length && response.errors[0];
                            var $mergeDialogContent = mergeDialog.$el.find('.aui-dialog2-content');

                            // if the PR is out of date, remove any existing error messages and hide the dialog
                            // and return so that the fallback error handling can show a dialog to reload the page
                            if (firstError.exceptionName === 'com.atlassian.bitbucket.pull.PullRequestOutOfDateException') {
                                $mergeDialogContent.find('.aui-message').remove();
                                mergeDialog.hide();
                                return;
                            }

                            if (firstError && (firstError.isConflicted || firstError.conflicted || firstError.vetoes && firstError.vetoes.length)) {
                                _events2.default.trigger('bitbucket.internal.pull-request.cant.merge', null, pullRequest, firstError.conflicted || firstError.isConflicted, firstError.vetoes);
                                $mergeDialogContent.children('.aui-message').remove();
                                $mergeDialogContent.prepend(bitbucket.internal.feature.pullRequest.merge.errors({ 'errors': response.errors }));
                                if (mergeDialog.$el.attr('aria-hidden') !== 'false') {
                                    _events2.default.trigger('bitbucket.internal.pull-request.show.cant.merge.help');
                                }
                                return false;
                            }
                        },
                        '*': function _() {
                            mergeDialog.hide();
                        }
                    },
                    timeout: (props.mergeTimeout || DEFAULT_MERGE_TIMEOUT_SEC) * 1000
                },
                callback: function callback(StashPullRequestJSON) {
                    (0, _updatePullRequest2.default)(_modelTransformer2.default.pullRequest(StashPullRequestJSON), true, props);
                    mergeDialog.hide();
                }
            };
            mergeDialog = createMergeDialog(options);
        });
    }

    // Creates a AUI Dialog2 dialog, separate from the legacy actionDialog which uses ConfirmDialog (AUI Dialog 1)
    function createMergeDialog(options) {
        var mergeDialog = _aui2.default.dialog2(bitbucket.internal.feature.pullRequest.merge.dialog(options.dialog));
        // we manually add the dialog to the body so that it's on the DOM and available for the branch deletion plugin
        // to disable the checkbox
        (0, _jquery2.default)('body').append(mergeDialog.$el);

        var mergeXhr = void 0;
        var promiseDecorator = void 0;
        var mergeStrategy = void 0;

        (0, _jquery2.default)(document).on('click', '#merge-strategy-opts aui-item-link', function (e) {
            var $target = (0, _jquery2.default)(e.currentTarget);
            mergeStrategy = $target.data('strategy');
            mergeDialog.$el.find(MERGE_STRATEGY_BUTTON_SELECTOR).text($target.find('.strategy-name').text());
            e.preventDefault();
        });

        mergeDialog.$el.find('.confirm-button').on('click', function (evt) {
            var spinnerTarget = (0, _jquery2.default)(MERGE_STRATEGY_BUTTON_SELECTOR).length ? (0, _jquery2.default)(MERGE_STRATEGY_BUTTON_SELECTOR) : evt.target;
            var spinner = new _submitSpinner2.default(spinnerTarget, 'before');

            (0, _setDialogButtonsDisabled2.default)(mergeDialog, true);
            spinner.show();

            mergeXhr = _ajax2.default.rest(babelHelpers.extends({}, options.ajax, {
                url: (0, _getActionUrl2.default)('merge'),
                type: 'POST',
                data: {
                    message: mergeDialog.$el.find('#commit-message').val(),
                    strategyId: mergeStrategy
                }
            }));

            var mergePromise = mergeXhr;

            mergeXhr.always(function () {
                mergeXhr = null; // null it out so that merge can't be cancelled below
                spinner.hide();
                (0, _setDialogButtonsDisabled2.default)(mergeDialog, false);
            });

            // HACK - we don't want to expose plugin points for the promise yet
            // we hard code a link to the branch deletion, if it's available
            if (!promiseDecorator) {
                try {
                    // assigning require to a variable - otherwise Babel will move the import to
                    // the top level require, removing the if !promiseDecorator condition. If the
                    // branchDeletion module is not available, this merge-dialog module will fail
                    var hideRequireFromBabel = require;
                    promiseDecorator = hideRequireFromBabel('pullRequest/branchDeletion').getMergePromiseDecorator;
                } catch (err) {
                    // ignore
                }
            }

            if (promiseDecorator) {
                mergePromise = promiseDecorator(mergePromise, function () {
                    return mergeDialog.hide();
                }) || mergePromise;
            }

            mergePromise.done(function (StashPullRequestJSON) {
                _events2.default.trigger('bitbucket.internal.feature.pullRequest.merged', null, {
                    user: _pageState2.default.getCurrentUser().toJSON(),
                    pullRequest: StashPullRequestJSON
                });
                options.callback(StashPullRequestJSON);
            });
        });

        mergeDialog.$el.find('.cancel-button').on('click', function () {
            if (mergeXhr) {
                mergeXhr.abort();
                mergeXhr = null;
            }

            collapseCommitMessage(mergeDialog);
            mergeDialog.hide();
        });

        mergeDialog.$el.find('.commit-message').on('focus', expandCommitMessage);

        mergeDialog.$el.find('.cancel-commit-message-link').click(function (e) {
            e.preventDefault();
            collapseCommitMessage(mergeDialog);
        });

        mergeDialog.on('hide', function () {
            collapseCommitMessage(mergeDialog);
        });

        mergeDialog.$el.on('click', '.view-merge-veto-details-button', function () {
            mergeDialog.hide();
            _events2.default.trigger('bitbucket.internal.pull-request.show.cant.merge.help');
        });

        return mergeDialog;
    }

    function onMergeClick() {
        mergeDialog.show();

        // Dialog2 automatically focuses the first focus-able element in the dialog, eg. the textarea, which
        // causes it to expand when the dialog is shown, so we re-add the .collapsed class to the form and
        // re-focus the Merge button instead.
        // Dialog2 docs mention a 'data-aui-focus-selector ' attribute to control what element receives focus
        // when the dialog is shown, but it appears to be ignored and overriden.
        // https://ecosystem.atlassian.net/browse/AUI-3299 to either re-implement it, or fix the docs
        collapseCommitMessage(mergeDialog);
        mergeDialog.$el.find('.confirm-button').focus();
    }

    function expandCommitMessage() {
        (0, _jquery2.default)(this).closest('.commit-message-form').removeClass('collapsed');
    }

    function collapseCommitMessage(mergedialog) {
        mergedialog.$el.find('.commit-message').val('');
        mergedialog.$el.find('.commit-message-form').addClass('collapsed');
    }

    exports.default = {
        onMergeClick: onMergeClick,
        initMergeDialog: initMergeDialog
    };
    module.exports = exports['default'];
});