define('bitbucket/internal/v2/feature/pull-request/header/get-action-url', ['module', 'exports', 'bitbucket/util/navbuilder', 'bitbucket/internal/model/page-state'], function (module, exports, _navbuilder, _pageState) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _navbuilder2 = babelHelpers.interopRequireDefault(_navbuilder);

    var _pageState2 = babelHelpers.interopRequireDefault(_pageState);

    exports.default = function (action) {

        var builder = _navbuilder2.default.rest().currentPullRequest()[action]().withParams({
            avatarSize: bitbucket.internal.widget.avatarSizeInPx({ size: 'xsmall' }),
            version: _pageState2.default.getPullRequest().getVersion()
        });

        return builder.build();
    };

    module.exports = exports['default'];
});