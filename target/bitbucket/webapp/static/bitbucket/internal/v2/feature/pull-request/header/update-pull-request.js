define('bitbucket/internal/v2/feature/pull-request/header/update-pull-request', ['module', 'exports', 'bitbucket/internal/model/page-state'], function (module, exports, _pageState) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _pageState2 = babelHelpers.interopRequireDefault(_pageState);

    exports.default = function (newPullRequestJSON, dispatch, props) {
        // also update the pageState for current legacy operations.
        if (newPullRequestJSON._stash) {
            _pageState2.default.getPullRequest().set(newPullRequestJSON._stash);
        }
        if (dispatch === true) {
            props.dispatch({ type: 'PR_SET_PULL_REQUEST', payload: newPullRequestJSON });
        }
    };

    module.exports = exports['default'];
});