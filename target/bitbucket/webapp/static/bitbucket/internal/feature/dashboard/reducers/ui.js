define('bitbucket/internal/feature/dashboard/reducers/ui', ['module', 'exports', 'lodash', 'bitbucket/internal/bbui/utils/create-reducer', '../actions', '../repository-type'], function (module, exports, _lodash, _createReducer2, _actions, _repositoryType) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _createReducer3 = babelHelpers.interopRequireDefault(_createReducer2);

    var _createReducer;

    var DEFAULT_REPOSITORY_LIST = _repositoryType.RECENT;

    /**
     * Handles load repositories optimistic action
     * @param {Object} state
     * @param {Object} action
     * @param {string?} action.meta.query - for a search, the search query
     * @param {RepositoryType} action.meta.repoType
     * @returns {Object}
     */
    function loadRepositories() {
        var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
        var action = arguments[1];

        var newState = babelHelpers.extends({}, state, {
            repositoriesLoading: true
        });

        // If we're loading repositories for a new query, then don't display any repos (just the spinner)
        if ((0, _lodash.get)(action, 'meta.query') !== (0, _lodash.get)(state, 'currentQuery')) {
            newState.activeRepositoryList = null;
        } else {
            newState.activeRepositoryList = (0, _lodash.get)(action, 'meta.repoType', null);
        }

        return newState;
    }

    /**
     * Handles load repository completion, and clear repository actions.
     * @param {Object} state
     * @param {Object} action
     * @param {string?} action.meta.query - for a search, the query triggered this action
     * @param {RepositoryType} action.meta.repoType - repostory list type that applies to this action
     * @returns {Object}
     */
    function updateRepositories() {
        var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
        var action = arguments[1];

        return babelHelpers.extends({}, state, {
            repositoriesLoading: false,
            currentQuery: (0, _lodash.get)(action, 'meta.query'),
            activeRepositoryList: action.type === _actions.CLEAR_REPOSITORIES ? DEFAULT_REPOSITORY_LIST : (0, _lodash.get)(action, 'meta.repoType')
        });
    }

    exports.default = (0, _createReducer3.default)({}, (_createReducer = {}, babelHelpers.defineProperty(_createReducer, _actions.LOAD_REPOSITORIES, loadRepositories), babelHelpers.defineProperty(_createReducer, _actions.LOAD_REPOSITORIES_FAILURE, updateRepositories), babelHelpers.defineProperty(_createReducer, _actions.LOAD_REPOSITORIES_SUCCESS, updateRepositories), babelHelpers.defineProperty(_createReducer, _actions.CLEAR_REPOSITORIES, updateRepositories), _createReducer));
    module.exports = exports['default'];
});