define('bitbucket/internal/feature/dashboard/reducers/repositories', ['module', 'exports', 'bitbucket/internal/bbui/utils/create-reducer', '../actions'], function (module, exports, _createReducer2, _actions) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _createReducer3 = babelHelpers.interopRequireDefault(_createReducer2);

    exports.default = (0, _createReducer3.default)({}, babelHelpers.defineProperty({}, _actions.LOAD_REPOSITORIES_SUCCESS, function (state, action) {
        var repos = action.payload.values.reduce(function (memo, repo) {
            if (state.hasOwnProperty(repo.id)) {
                memo[repo.id] = babelHelpers.extends({}, state[repo.id], repo);
            } else {
                memo[repo.id] = repo;
            }

            return memo;
        }, {});

        return babelHelpers.extends({}, state, repos);
    }));
    module.exports = exports['default'];
});