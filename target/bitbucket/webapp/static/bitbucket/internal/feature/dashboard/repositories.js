define('bitbucket/internal/feature/dashboard/repositories', ['exports', 'react', 'aui', 'lodash', 'react-redux', 'redux', 'bitbucket/internal/bbui/aui-react/spinner', './action-creators/load-recent-repositories', './components/repository-list', './components/repository-search'], function (exports, _react, _aui, _lodash, _reactRedux, _redux, _spinner, _loadRecentRepositories, _repositoryList, _repositorySearch) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.Repositories = undefined;

    var _react2 = babelHelpers.interopRequireDefault(_react);

    var _aui2 = babelHelpers.interopRequireDefault(_aui);

    var _spinner2 = babelHelpers.interopRequireDefault(_spinner);

    var _loadRecentRepositories2 = babelHelpers.interopRequireDefault(_loadRecentRepositories);

    var _repositoryList2 = babelHelpers.interopRequireDefault(_repositoryList);

    var _repositorySearch2 = babelHelpers.interopRequireDefault(_repositorySearch);

    var Repositories = exports.Repositories = function (_Component) {
        babelHelpers.inherits(Repositories, _Component);

        function Repositories() {
            babelHelpers.classCallCheck(this, Repositories);
            return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(Repositories).apply(this, arguments));
        }

        babelHelpers.createClass(Repositories, [{
            key: 'componentWillMount',
            value: function componentWillMount() {
                this.props.loadRecentRepositories();
            }
        }, {
            key: 'onMoreClick',
            value: function onMoreClick() {
                if (this.props.loadMore) {
                    this.props.loadMore();
                } else {
                    this.props.loadRecentRepositories(this.props.nextStart);
                }
            }
        }, {
            key: 'render',
            value: function render() {
                var _this2 = this;

                return _react2.default.createElement(
                    'div',
                    { id: 'dashboard-repositories', className: 'dashboard-repositories' },
                    _react2.default.createElement(
                        'h3',
                        null,
                        _aui2.default.I18n.getText('bitbucket.web.dashboard.repositories.title')
                    ),
                    _react2.default.createElement(_repositorySearch2.default, null),
                    _react2.default.createElement(_repositoryList2.default, {
                        repositories: this.props.repositories,
                        showMore: !this.props.isLastPage && !this.props.isLoading,
                        onMoreClick: function onMoreClick() {
                            return _this2.onMoreClick();
                        }
                    }),
                    this.props.isLoading ? _react2.default.createElement(_spinner2.default, null) : null
                );
            }
        }]);
        return Repositories;
    }(_react.Component);

    Repositories.propTypes = {
        loadRecentRepositories: _react.PropTypes.func.isRequired
    };

    function mapDispatchToProps(dispatch) {
        return (0, _redux.bindActionCreators)({
            loadRecentRepositories: _loadRecentRepositories2.default
        }, dispatch);
    }

    function mapStateToProps(state) {
        var props = {
            isLoading: state.ui.repositoriesLoading,
            isLastPage: true
        };

        // TODO - extract to a selector function to allow for testing
        var pagingInfo = (0, _lodash.get)(state.paging, state.ui.activeRepositoryList);
        if (pagingInfo) {
            props.repositories = pagingInfo.repositories.map(function (key) {
                return state.repositories[key];
            });
            if (pagingInfo.loadMoreCallback) {
                props.loadMore = pagingInfo.loadMoreCallback;
            } else {
                props.nextStart = pagingInfo.lastPageMeta.nextStart;
            }
            props.isLastPage = pagingInfo.lastPageMeta.isLastPage;
        }

        return props;
    }

    exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(Repositories);
});