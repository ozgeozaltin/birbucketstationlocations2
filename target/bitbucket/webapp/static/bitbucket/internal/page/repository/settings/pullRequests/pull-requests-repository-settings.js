define('bitbucket/internal/page/repository/settings/pullRequests/pull-requests-repository-settings', ['module', 'exports', 'aui', 'jquery'], function (module, exports, _aui, _jquery) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _aui2 = babelHelpers.interopRequireDefault(_aui);

    var _jquery2 = babelHelpers.interopRequireDefault(_jquery);

    function initMergeStrategies() {
        var $mergeStrategies = (0, _jquery2.default)('.merge-strategies');
        var mergeStrategy = '.merge-strategy';
        var defaultMergeStrategy = '.default-merge-strategy';
        var defaultMergeChecked = 'default-merge-checked';
        var defaultMergeStrategyLabel = '.default-merge-strategy-label';
        var mergeSettingsError = 'merge-settings-error';

        $mergeStrategies.on('change', mergeStrategy, function (evt) {
            var $defaultRadio = (0, _jquery2.default)(evt.target).siblings(defaultMergeStrategy);

            // toggle disable attr if the strategy is/not checked
            $defaultRadio.attr('disabled', !evt.target.checked);

            // uncheck the default radio if the strategy is unselected
            if (!evt.target.checked) {
                $defaultRadio.prop('checked', false);
            }

            var $checkedStrategies = $mergeStrategies.find(mergeStrategy + ':checked');
            // if there's just one checked strategy, set it as the default
            if ($checkedStrategies.length === 1) {
                $checkedStrategies.siblings(defaultMergeStrategy).click();
            }
        });
        $mergeStrategies.on('change', defaultMergeStrategy, function (evt) {
            // reset all default labels
            $mergeStrategies.find(defaultMergeStrategyLabel).removeClass(defaultMergeChecked).text(_aui2.default.I18n.getText('bitbucket.web.repository.settings.pullrequest.mergestrategy.set.default'));

            // update this label as the default
            (0, _jquery2.default)(evt.target).next(defaultMergeStrategyLabel).addClass(defaultMergeChecked).text('(' + _aui2.default.I18n.getText('bitbucket.web.repository.settings.pullrequest.mergestrategy.default') + ')');
        });

        function printErrorMsg($errorsContainer, test) {
            if (!$errorsContainer.find('#' + test.id).length) {
                $errorsContainer.append('<p id="' + test.id + '" class="' + mergeSettingsError + '">' + test.message + '</p>');
            }
            return false;
        }

        function removeErrorMsg($errorsContainer, test) {
            $errorsContainer.find('#' + test.id).remove();
            return true;
        }

        function validate($errorsContainer, test) {
            return test.condition ? removeErrorMsg($errorsContainer, test) : printErrorMsg($errorsContainer, test);
        }

        (0, _jquery2.default)('form.pull-request-settings').on('submit', function (evt) {
            var $errorsContainer = (0, _jquery2.default)('#merge-strategy-errors');
            var testStrategy = {
                id: 'enabledUnset',
                message: _aui2.default.I18n.getText('bitbucket.web.repository.settings.pullrequest.mergestrategy.invalid.selection'),
                condition: $mergeStrategies.find(mergeStrategy + ':checked').length
            };
            var testDefault = {
                id: 'defaultUnset',
                message: _aui2.default.I18n.getText('bitbucket.web.repository.settings.pullrequest.mergestrategy.invalid.default'),
                condition: $mergeStrategies.find(defaultMergeStrategy + ':checked').length
            };

            $errorsContainer.empty();

            var valid = [testStrategy, testDefault].every(function (test) {
                return validate($errorsContainer, test);
            });

            if (!valid) {
                evt.preventDefault();
            }
        });
    }

    function onReady(_ref) {
        var showMergeStrategies = _ref.showMergeStrategies;

        if (showMergeStrategies) {
            initMergeStrategies();
        }
    }

    exports.default = {
        onReady: onReady
    };
    module.exports = exports['default'];
});